-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2022 at 09:53 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistemts`
--

-- --------------------------------------------------------

--
-- Table structure for table `tabel_access_token`
--

CREATE TABLE `tabel_access_token` (
  `id_token` int(20) NOT NULL,
  `id_akun` int(20) NOT NULL,
  `access_token` varchar(500) NOT NULL,
  `ip_address` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_access_token`
--

INSERT INTO `tabel_access_token` (`id_token`, `id_akun`, `access_token`, `ip_address`) VALUES
(9, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjozLCJpZF9wZW5nZ3VuYSI6MywiaWRlbnRpdGFzX3BlbmdndW5hIjoiMTIzNDMxMjMxMjMiLCJwYXNzd29yZCI6IiQyYiQxMiRXQk1tZlpHZFNTLjNvRDliMy5NZENPV1FFSGRxNlFIdjI0ZWx1RUdUNFROV242ckhlZFhUNiIsInJvbGUiOjMsInN0YXR1cyI6MCwidHlwZSI6Ik5JUyJ9XSwiaWF0IjoxNjM0NTI5NTkxLCJleHAiOjE2MzQ1MzUzNTF9.017Uu4KRyrelLVQhW1qZSR70XfhcwAgxmyLF4m08em4', '10.2.14.120'),
(10, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo0LCJpZF9wZW5nZ3VuYSI6NCwiZW1haWwiOiI4OTczNDIzNDI3NzMyIiwicGFzc3dvcmQiOiIkMmIkMTIkQVF0RlNzaUp0b0VqbWpNMTByMWhSLi9YR3RaNEQxVHk0amluU0sxZ1JRSzN5NEhaUlYySXEiLCJyb2xlIjoxLCJzdGF0dXMiOjB9XSwiaWF0IjoxNjM5NDY4MTc5LCJleHAiOjE2Mzk0NzM5Mzl9.kSnVFmKrmJY0cuJMTweQ_J4Zr008TkjJRfZ5hkRXtHc', '192.168.43.71'),
(18, 5, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo1LCJpZF9wZW5nZ3VuYSI6MSwiZW1haWwiOiIxOTYzMDMxMTE5ODYwMzIwMDgiLCJwYXNzd29yZCI6IiQyYiQxMiRPZDRtTWt3djRmMXB6RWhZREw4Y3lPRWRDQWpQUWNxWVA0MkpiZTAuSFB5b1RyeWtwQ3ZLSyIsInJvbGUiOjIsInN0YXR1cyI6MH1dLCJpYXQiOjE2Mzk0Njg1NTYsImV4cCI6MTYzOTQ3NDMxNn0.emLP4AT16XfwQmZgOBmHbdPPyZCQyZ9ftDKd9UCmWbQ', '192.168.43.71'),
(19, 6, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo2LCJpZF9wZW5nZ3VuYSI6NiwiZW1haWwiOiJhZ3VzZHVtbXlwcm9qZWtAZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmIkMTIkT3k3U0NNZ1VrQVdJb0NPTVV4NUlzdTZaSkk5TW1hZ0JJZXJ2RnFvZFJLbGJSUUZpTWViTUMiLCJyb2xlIjoyLCJzdGF0dXMiOjB9XSwiaWF0IjoxNjQyMDY2OTUyLCJleHAiOjE2NDIwNzI3MTJ9.SEczJT4COHyT_63MU6jYvSO8lS56Jv2XoVAQRJDko1g', '172.17.128.1'),
(20, 9, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjo5LCJpZF9wZW5nZ3VuYSI6OSwiZW1haWwiOiJhemlzQGdtYWlsLmNvbSIsInBhc3N3b3JkIjoiJDJiJDEyJFM3eUkxWmVpRXN5ZzhKV0dCRnRZVS5PWFIzdGU3alJjWXV0MGhuLkcuSktxZkVJLmVuNVYuIiwicm9sZSI6Miwic3RhdHVzIjowfV0sImlhdCI6MTY0MjA4NzU0NiwiZXhwIjoxNjQyMDkzMzA2fQ.W5gpGTj2LEipvpNHHcIH2puIdPetKoi68pzjPKygDHE', '192.168.43.71'),
(21, 10, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb3dzIjpbeyJpZF9ha3VuIjoxMCwiaWRfcGVuZ2d1bmEiOjUsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmIkMTIkZDNqOEU3RHNMMlg1TGpYRjBiUzVFdTZmL3FoWkVZbUlJR3JVZHJGcHc0UzVGMDVpaHo0NGEiLCJyb2xlIjoxLCJzdGF0dXMiOjB9XSwiaWF0IjoxNjQyMTY1OTIwLCJleHAiOjE2NDIxNzE2ODB9.np_EcENaPYkT3VzMPegYwdMsoV4h2TH7kIoRFZ6DtX0', '172.26.224.1');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_admin`
--

CREATE TABLE `tabel_admin` (
  `id_admin` int(2) NOT NULL,
  `nama_admin` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tabel_admin`
--

INSERT INTO `tabel_admin` (`id_admin`, `nama_admin`, `email`) VALUES
(1, 'admin', ''),
(4, 'adminmaster', '8973423427732'),
(5, 'admin', 'admin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_agenda`
--

CREATE TABLE `tabel_agenda` (
  `id_agenda` int(20) NOT NULL,
  `nama_agenda` varchar(500) NOT NULL,
  `bidang` int(20) NOT NULL,
  `foto_kegiatan` varchar(500) NOT NULL,
  `foto_pengurus` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_agenda`
--

INSERT INTO `tabel_agenda` (`id_agenda`, `nama_agenda`, `bidang`, `foto_kegiatan`, `foto_pengurus`) VALUES
(3, 'agenda 3', 1, 'foto_kegiatan_1642084162501.png', 'foto_pengurus_1642084162506.png'),
(4, 'agenda1', 2, 'foto_kegiatan_1642085716409.png', 'foto_pengurus_1642085716759.png');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_akun`
--

CREATE TABLE `tabel_akun` (
  `id_akun` int(20) NOT NULL,
  `id_pengguna` int(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(500) NOT NULL,
  `role` int(2) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_akun`
--

INSERT INTO `tabel_akun` (`id_akun`, `id_pengguna`, `email`, `password`, `role`, `status`) VALUES
(4, 4, '8973423427732', '', 1, 0),
(5, 1, '196303111986032008', '$2b$12$Od4mMkwv4f1pzEhYDL8cyOEdCAjPQcqYP42Jbe0.HPyoTrykpCvKK', 2, 0),
(6, 6, 'agusdummyprojek@gmail.com', '$2b$12$Oy7SCMgUkAWIoCOMUx5Isu6ZJI9MmagBIervFqodRKlbRQFiMebMC', 2, 0),
(9, 9, 'azis@gmail.com', '$2b$12$S7yI1ZeiEsyg8JWGBFtYU.OXR3te7jRcYut0hn.G.JKqfEI.en5V.', 2, 0),
(10, 5, 'admin@gmail.com', '$2b$12$d3j8E7DsL2X5LjXF0bS5Eu6f/qhZEYmIIGrUdrFpw4S5F05ihz44a', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_bidang`
--

CREATE TABLE `tabel_bidang` (
  `id_bidang` int(20) NOT NULL,
  `nama_bidang` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_bidang`
--

INSERT INTO `tabel_bidang` (`id_bidang`, `nama_bidang`) VALUES
(1, 'Bidang 1 (Organisasi dan Pengkaderan)'),
(2, 'Bidang 2 (Kepelatihan dan Pembinaan Prestasi)'),
(3, 'Bidang 3 (Kominfo, Pemberdayaan Masyarakat dan Kewirausahaan)');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_jadwal`
--

CREATE TABLE `tabel_jadwal` (
  `id_jadwal` int(20) NOT NULL,
  `waktu` varchar(500) NOT NULL,
  `tempat` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_jadwal`
--

INSERT INTO `tabel_jadwal` (`id_jadwal`, `waktu`, `tempat`) VALUES
(2, '2022-01-18 19.00', 'FT'),
(3, '2022-01-18 19.00', 'FKIP');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_penerimaan_biodata_kejuaraan`
--

CREATE TABLE `tabel_penerimaan_biodata_kejuaraan` (
  `id_kejuaraan` int(20) NOT NULL,
  `id_user` int(20) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `file` varchar(500) NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_penerimaan_biodata_kejuaraan`
--

INSERT INTO `tabel_penerimaan_biodata_kejuaraan` (`id_kejuaraan`, `id_user`, `tgl_daftar`, `file`, `status`) VALUES
(2, 6, '2022-01-02', 'file_1641121762217.pdf', 1),
(10, 6, '2022-01-12', 'file_1641956273974.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_penerimaan_kader`
--

CREATE TABLE `tabel_penerimaan_kader` (
  `id_kader` int(20) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `file` varchar(500) NOT NULL,
  `id_user` int(20) NOT NULL,
  `status` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_penerimaan_kader`
--

INSERT INTO `tabel_penerimaan_kader` (`id_kader`, `tgl_daftar`, `file`, `id_user`, `status`) VALUES
(1, '2022-01-04', 'file_1641265922656.pdf', 6, 1),
(5, '2022-01-12', 'file_1641957255962.pdf', 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_user`
--

CREATE TABLE `tabel_user` (
  `id_user` int(20) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `tempat_tgl_lahir` varchar(500) NOT NULL,
  `alamat` text NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `agama` varchar(100) NOT NULL,
  `tinggi_badan` varchar(200) NOT NULL,
  `fakultas_jurusan` varchar(500) NOT NULL,
  `nim` varchar(500) NOT NULL,
  `nama_ayah` varchar(200) NOT NULL,
  `nama_ibu` varchar(200) NOT NULL,
  `status` int(20) NOT NULL,
  `berat_badan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tabel_user`
--

INSERT INTO `tabel_user` (`id_user`, `nama_user`, `email`, `no_hp`, `tgl_daftar`, `tempat_tgl_lahir`, `alamat`, `jenis_kelamin`, `agama`, `tinggi_badan`, `fakultas_jurusan`, `nim`, `nama_ayah`, `nama_ibu`, `status`, `berat_badan`) VALUES
(3, 'agus', '196303111986032008', '0877423', '0000-00-00', 'jakarta;19-02-1998', 'jakarta', 'L', 'Islam', '170cm', 'Teknik', '98342342', 'coba', 'coba1', 1, ''),
(6, 'agus2', 'agusdummyprojek@gmail.com', '98543534', '2021-12-15', 'jakarta;05-11-1996; ', 'jakarta', 'L', 'Islam', '170cm', 'Teknik', '98435234', 'agus', 'ada', 1, '72kg'),
(9, 'azis', 'azis@gmail.com', '9934234', '0000-00-00', 'Jakarta; ', 'jlan alamat', 'L', 'Islam', '170cm', 'Teknik', '873342342', 'ayah', 'ibu', 1, '68kg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabel_access_token`
--
ALTER TABLE `tabel_access_token`
  ADD PRIMARY KEY (`id_token`);

--
-- Indexes for table `tabel_admin`
--
ALTER TABLE `tabel_admin`
  ADD PRIMARY KEY (`id_admin`),
  ADD UNIQUE KEY `nip` (`email`);

--
-- Indexes for table `tabel_agenda`
--
ALTER TABLE `tabel_agenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indexes for table `tabel_akun`
--
ALTER TABLE `tabel_akun`
  ADD PRIMARY KEY (`id_akun`),
  ADD UNIQUE KEY `identitas_pengguna` (`email`);

--
-- Indexes for table `tabel_bidang`
--
ALTER TABLE `tabel_bidang`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indexes for table `tabel_jadwal`
--
ALTER TABLE `tabel_jadwal`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `tabel_penerimaan_biodata_kejuaraan`
--
ALTER TABLE `tabel_penerimaan_biodata_kejuaraan`
  ADD PRIMARY KEY (`id_kejuaraan`);

--
-- Indexes for table `tabel_penerimaan_kader`
--
ALTER TABLE `tabel_penerimaan_kader`
  ADD PRIMARY KEY (`id_kader`);

--
-- Indexes for table `tabel_user`
--
ALTER TABLE `tabel_user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `nis` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabel_access_token`
--
ALTER TABLE `tabel_access_token`
  MODIFY `id_token` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tabel_admin`
--
ALTER TABLE `tabel_admin`
  MODIFY `id_admin` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tabel_agenda`
--
ALTER TABLE `tabel_agenda`
  MODIFY `id_agenda` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tabel_akun`
--
ALTER TABLE `tabel_akun`
  MODIFY `id_akun` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tabel_bidang`
--
ALTER TABLE `tabel_bidang`
  MODIFY `id_bidang` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tabel_jadwal`
--
ALTER TABLE `tabel_jadwal`
  MODIFY `id_jadwal` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tabel_penerimaan_biodata_kejuaraan`
--
ALTER TABLE `tabel_penerimaan_biodata_kejuaraan`
  MODIFY `id_kejuaraan` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tabel_penerimaan_kader`
--
ALTER TABLE `tabel_penerimaan_kader`
  MODIFY `id_kader` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tabel_user`
--
ALTER TABLE `tabel_user`
  MODIFY `id_user` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
