const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = process.env.PORT || 3000;
const morgan = require('morgan');
const koneksi = require('./koneksi');
const cors = require('cors');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors());
app.use('/controller', require('./middleware'));

app.listen(PORT, () => {
    console.log(`Server started on port`+PORT);
});