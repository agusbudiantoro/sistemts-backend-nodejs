const express = require('express');
const multer = require("multer");
const path = require("path");
const router = express.Router();

const auth = require('./controller/auth');
const authVerif = require('./verifikasi');

const registerAkunUser = require('./controller/registerAkun/akunUser');
const registerAkunAdmin = require('./controller/registerAkun/akunAdmin');
const getUser = require('./controller/registerAkun/get'); 

//tabel penerimaan biodata
const postPenerimaanBiodata = require('./controller/controller_penerimaan_biodata/post');
const getPenerimaanBiodata = require('./controller/controller_penerimaan_biodata/get');
const deletePenerimaanBiodata = require('./controller/controller_penerimaan_biodata/delete');
const putPenerimaanBiodata = require('./controller/controller_penerimaan_biodata/put');

//tabel penerimaan kader
const postPenerimaanKader = require('./controller/controller_penerimaan_kader/post');
const getPenerimaanKader = require('./controller/controller_penerimaan_kader/get');
const deletePenerimaanKader = require('./controller/controller_penerimaan_kader/delete');
const putPenerimaanKader = require('./controller/controller_penerimaan_kader/put');

//tabel agenda
const postAgenda = require('./controller/controller_agenda/post');
const getAgenda = require('./controller/controller_agenda/get');
const deleteAgenda = require('./controller/controller_agenda/delete');

// tabel jadwal
const postJadwal = require('./controller/controller_jadwal/post');
const getJadwal = require('./controller/controller_jadwal/get');
const deleteJadwal = require('./controller/controller_jadwal/delete');
const editJadwal = require('./controller/controller_jadwal/put');

// tabel bidang
const getBidang  = require('./controller/controller_bidang/get');

//download file
const downloadFile = require('./controller/download/index');

const storage = multer.diskStorage({
    destination: './upload/file',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});

const storageFoto = multer.diskStorage({
    destination: './upload/foto',
    filename: (req, file, cb) => {
        return cb(null, `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`);
    }
});

var upload = multer({
    storage: storage,
    limits: { fileSize: 10000000 }
})

var uploadGambar = multer({
    storage: storageFoto,
    limits: { fileSize: 10000000 }
})
router.use(express.static('public'));
router.use('/file', express.static('./upload/file'),(req,res)=>{
    console.log("cek");
});

router.use('/foto', express.static('./upload/foto'),(req,res)=>{
    console.log("cek");
});

router.post('/api/v1/login', auth.login);
router.post('/api/v1/cekOauthToken', authVerif.verifikasiGeneral(),auth.cekToken);
router.post('/api/v1/registerAkun',registerAkunUser.registeruser);
router.post('/api/v1/registerAdmin',registerAkunAdmin.registerAdminTu);
router.get('/api/v1/getUserById/:id', getUser.getUserById);
router.get('/api/v1/getAllUser',registerAkunUser.getAllUser);
router.put('/api/v1/updateUser/:id', registerAkunUser.editUser)

router.put('/api/v1/verifUser/:id', registerAkunUser.verifUser);

// tabel penerimaan biodata
router.post('/api/v1/tambahBiodata', upload.single("file"),postPenerimaanBiodata.tambahBiodata);
router.get('/api/v1/getBiodataByIdUser/:id_user', getPenerimaanBiodata.getByIdUser);
router.delete('/api/v1/deleteBiodata/:id_kejuaraan', deletePenerimaanBiodata.deleteById);
router.put('/api/v1/editBiodata/:id_kejuaraan', upload.single("file"),putPenerimaanBiodata.putBiodata);
router.put('/api/v1/editStatusBiodata/:id_kejuaraan', putPenerimaanBiodata.putStatusBiodata);
router.get('/api/v1/getAllBiodata', getPenerimaanBiodata.getAll);

// tabel penerimaan kader
router.post('/api/v1/tambahKader', upload.single("file"),postPenerimaanKader.tambahkader);
router.get('/api/v1/getKaderByIdUser/:id_user', getPenerimaanKader.getByIdUser);
router.delete('/api/v1/deleteKader/:id_kader', deletePenerimaanKader.deleteById);
router.put('/api/v1/editKader/:id_kader', upload.single("file"),putPenerimaanKader.putkader);
router.put('/api/v1/editStatusKader/:id_kader', putPenerimaanKader.putStatuskader);
router.get('/api/v1/getAllKader', getPenerimaanKader.getAll);

//tabel agenda
router.post('/api/v1/tambahAgenda', uploadGambar.fields([{name:"foto_kegiatan"},{name:"foto_pengurus"}]),postAgenda.tambahAgenda);
router.get('/api/v1/tampilAgenda', getAgenda.getAllAgenda);
router.get('/api/v1/tampilAgendaByIdBidang/:id', getAgenda.getAgendaByIdBidang);
router.delete('/api/v1/hapusAgenda/:id_agenda', deleteAgenda.deleteById);

// tabel bidang
router.get('/api/v1/tampilBidang', getBidang.getAllBidang);

//tabel jadwal
router.post('/api/v1/tambahJadwal',postJadwal.tambahJadwal);
router.get('/api/v1/tampilJadwal', getJadwal.getAllJadwal);
router.delete('/api/v1/deleteJadwal/:id_jadwal', deleteJadwal.deleteById);
router.put('/api/v1/editJadwal/:id_jadwal', editJadwal.editJadwal);

//download file
router.get('/api/v1/download/:namefile', downloadFile.downloadFile);

module.exports = router