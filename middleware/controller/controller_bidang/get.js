const model_bidang = require('../../model/model_bidang/get');
const model_agenda = require('../../model/model_agenda/get');

exports.getAllBidang = async function (req,res){
    let dataJadi =[];
    let resBidang = await model_bidang.getBidang(res);
    console.log(resBidang);
    for(var i =0;i<resBidang.length;i++){
        let resAgenda = await model_agenda.getCountAgendaByIdBidang(resBidang[i].id_bidang,res);
        dataJadi.push({id_bidang:resBidang[i].id_bidang, nama_bidang:resBidang[i].nama_bidang,jumlah_agenda:resAgenda.length});
    }
    res.status(200).json(dataJadi);
}