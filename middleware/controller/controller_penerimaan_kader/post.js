const model_penerimaan_data = require('../../model/model_penerimaan_kader/post');

exports.tambahkader = async function (req,res){
    let date = new Date();
    let year = date.getFullYear();
    let month = date.getMonth()+1;
    let day = date.getDate();
    console.log(req.file);

    let body = {
        id_user:req.body.id_user,
        tgl_daftar:year+"-"+month+"-"+day,
        status:0,
        file:req.file.filename
    }
    return model_penerimaan_data.tambahkader(res, body);
}