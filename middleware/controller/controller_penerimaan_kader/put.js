const model_penerimaan_kader = require('../../model/model_penerimaan_kader/put');

exports.putkader = async function (req,res){
    let body ={
        id_kader:req.params.id_kader,
        file:req.file.filename,
    }

    return model_penerimaan_kader.editkader(res,body);
}

exports.putStatuskader = async function (req,res){
    let body ={
        id_kader:req.params.id_kader
    }

    return model_penerimaan_kader.editStatuskader(res,body);
}