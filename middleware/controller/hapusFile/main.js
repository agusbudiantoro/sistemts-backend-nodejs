const pathHapus = './upload/file/';
const fs = require('fs');

exports.hapusFile = function(filename){
    return new Promise(async function(resolve,reject){
        try {
            await fs.unlinkSync(pathHapus+filename);
            resolve({status:true,value:[]});
        } catch (error) {
            reject({status:false,value:error});
        }
    })
}