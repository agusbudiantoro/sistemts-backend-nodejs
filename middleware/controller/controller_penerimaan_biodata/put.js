const model_penerimaan_biodata = require('../../model/model_penerimaan_biodata/put');

exports.putBiodata = async function (req,res){
    let body ={
        id_kejuaraan:req.params.id_kejuaraan,
        file:req.file.filename,
    }

    return model_penerimaan_biodata.editBiodata(res,body);
}

exports.putStatusBiodata = async function (req,res){
    let body ={
        id_kejuaraan:req.params.id_kejuaraan
    }

    return model_penerimaan_biodata.editStatusBiodata(res,body);
}