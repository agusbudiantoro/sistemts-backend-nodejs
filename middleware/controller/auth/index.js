const connection = require('../../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../../res');
const jwt = require('jsonwebtoken');
const config = require('../../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

const tabel_user = require('../../model/tabel_user');
const tabel_access_token = require('../../model/tabel_access_token');
const tabel_admin = require('../../model/tabel_admin');

exports.login = async function(req,res){
    let post = {
        "email":req.body.email,
        "password":req.body.password
    }
    console.log(post);
 
    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["tabel_akun", "email", post.email];

    query = mysql.format(query, table);

    connection.query(query, function (error, rows) {
        if(error){
            res.status(404).send("gagal login");
        } else {
            if(rows.length != 0){
                bcrypt.compare(post.password, rows[0].password, async function (err, result) {
                    if(result == true){
                        var token = jwt.sign({ rows }, config.secret, {
                            expiresIn: 5760
                        });
                        
                        if(rows[0].role == 1){
                            let identitas = {
                                email:post.email
                            }
                            let resGetDataAdmin = await tabel_admin.getDataAdmin(res,identitas);
                            let resFinalDataAdmin = await resGetDataAdmin;
                            let resCombine = {
                                "id_pengguna": resFinalDataAdmin[0].id_admin,
                                "nama": resFinalDataAdmin[0].nama_admin,
                                "token": token,
                                "email":resFinalDataAdmin[0].email,
                                "ip_address":ip.address(),
                                "role":rows[0].role,
                                "status":1
                            }
                            let resGetAccessToken = await tabel_access_token.getAccessToken(res,resCombine,rows[0]);
                            let resPromiseAccessToken = await resGetAccessToken;
                            if(resPromiseAccessToken.length == 0){
                                await tabel_access_token.insertAccessToken(res,resCombine,rows[0]);
                            } else{
                                await tabel_access_token.updateAccessToken(res, resCombine,resPromiseAccessToken[0])
                            }
                        }
                        else if(rows[0].role == 2){
                            let identitas = {
                                email:post.email
                            }
                            let resGetDataUser = await tabel_user.getDataUser(res,identitas);
                            let resFinalDataUser = await resGetDataUser;
                            let resCombine = {
                                "id_pengguna": resGetDataUser[0].id_user,
                                "nim": resGetDataUser[0].nim,
                                "nama": resGetDataUser[0].nama_user,
                                "email":resGetDataUser[0].email,
                                "tempat_tgl_lahir": resGetDataUser[0].tempat_tgl_lahir,
                                "no_hp": resGetDataUser[0].no_hp,
                                "tgl_daftar":resGetDataUser[0].tgl_daftar,
                                "alamat": resGetDataUser[0].alamat,
                                "jenis_kelamin":resGetDataUser[0].jenis_kelamin,
                                "agama":resGetDataUser[0].agama,
                                "tinggi_berat_badan":resGetDataUser[0].tinggi_berat_badan,
                                "fakultas_jurusan":resGetDataUser[0].fakultas_jurusan,
                                "nama_ayah":resGetDataUser[0].nama_ayah,
                                "nama_ibu":resGetDataUser[0].nama_ibu,
                                "status": resGetDataUser[0].status,
                                "token": token,
                                "ip_address":ip.address(),
                                "role":rows[0].role
                            }
                            
                            let resGetAccessToken = await tabel_access_token.getAccessToken(res,resCombine,rows[0]);
                            let resPromiseAccessToken = await resGetAccessToken;
                            if(resPromiseAccessToken.length == 0){
                                await tabel_access_token.insertAccessToken(res,resCombine,rows[0]);
                            } else{
                                await tabel_access_token.updateAccessToken(res, resCombine,resPromiseAccessToken[0])
                            }
                        }
                    } else {
                        res.status(404).send("gagal login, password salah");
                    }
                });
            } else {
                res.status(404).send({
                    message:"gagal login, email salah"
                });
            }
        }
    })
}

exports.cekToken = function (req, res) {
    response.ok("Token Tersedia", res);
}