const model_agenda = require('../../model/model_agenda/delete');

exports.deleteById = async function (req,res){
    return model_agenda.hapusAgenda(res,req.params.id_agenda);
}