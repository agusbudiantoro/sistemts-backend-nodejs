const model_agenda = require('../../model/model_agenda/post');

exports.tambahAgenda = async function (req,res){
    let body = {
        nama_agenda:req.body.nama_agenda,
        bidang:req.body.bidang,
        foto_kegiatan:req.files.foto_kegiatan[0].filename,
        foto_pengurus:req.files.foto_pengurus[0].filename
    }
    return model_agenda.tambahAgenda(res, body);
}