const model_agenda = require('../../model/model_agenda/get');

exports.getAllAgenda = async function (req,res){
    return model_agenda.getAgenda(res);
}

exports.getAgendaByIdBidang = async function(req,res){
    let resAgenda = await model_agenda.getCountAgendaByIdBidang(req.params.id,res);
    return res.status(200).json(resAgenda);
}