const model_jadwal = require('../../model/model_jadwal/put');

exports.editJadwal = async function (req,res){
    console.log(req.body);
    let body = {
        id_jadwal:req.params.id_jadwal,
        waktu:req.body.waktu,
        tempat:req.body.tempat
    }
    return model_jadwal.editJadwal(res, body);
}