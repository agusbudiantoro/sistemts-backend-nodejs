const model_user = require('../../model/tabel_user');

exports.getUserById = async function(req,res){
    return model_user.getDataUserById(res, req.params.id);
}