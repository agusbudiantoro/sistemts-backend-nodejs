const connection = require('../../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../../res');
const jwt = require('jsonwebtoken');
const config = require('../../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

const tabel_user = require('../../model/tabel_user');
const tabel_akun = require('../../model/tabel_akun');

exports.registeruser = async function (req, res) {
        let date = new Date();
        let tgl = date.getDate();
        let bulan = date.getMonth()+1;
        let tahun = date.getFullYear();

        var post = {
            nama_user:req.body.nama_user,
            email:req.body.email
        }
        let password=req.body.password;
        let resDatauser='';
        let resPromiseDatauser='';
        try {
            resDatauser = await tabel_user.getDataUser(res,post); 
            resPromiseDatauser = await resDatauser;  
        } catch (error) {
            res.status(404).send(error);
        
        }
        if(resPromiseDatauser.length == 0){
            let resInsertuser = await tabel_user.insertDataUser(res,post);
            let resPromiseInsertuser = await resInsertuser;

            let resGetAkunuser = await tabel_akun.getDataAkun(res,post);
            let resPromiseGetAkunuser = await resGetAkunuser;
            let idPengguna = await resPromiseInsertuser.insertId
            if(resPromiseGetAkunuser.length == 0){
                return tabel_akun.registerAkun(res,post,2, password, idPengguna);
            } else{ 
                return tabel_akun.updateIdAkun(res,post,2,password,idPengguna)
            }
        } else {
            res.status(405).send("data sudah ada");
        }
}

exports.editUser = async function (req, res) {
    let date = new Date();
    let tgl = date.getDate();
    let bulan = date.getMonth()+1;
    let tahun = date.getFullYear();
    console.log(req.params.id);
    var post = {
        id_user:req.params.id,
        nama_user:req.body.nama_user,
        email:req.body.email,
        no_hp:req.body.no_hp,
        tempat_tgl_lahir:req.body.tempat_tgl_lahir,
        alamat:req.body.alamat,
        jenis_kelamin:req.body.jenis_kelamin,
        agama:req.body.agama,
        tinggi_badan:req.body.tinggi_badan,
        fakultas_jurusan:req.body.fakultas_jurusan,
        nim:req.body.nim,
        nama_ayah:req.body.nama_ayah,
        nama_ibu:req.body.nama_ibu,
        status:req.body.status,
        berat_badan:req.body.berat_badan
    }
    return await tabel_user.updateUser(res,post); 
}

exports.verifUser = async function(req,res){
    return tabel_user.verifUser(res,req.params.id);
}

exports.getAllUser = async function(req,res){
    return tabel_user.getAllDataUser(res);
}