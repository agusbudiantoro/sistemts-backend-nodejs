const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getBiodataByIdUser = async function(res,id_user){

    var query = "SELECT * FROM ?? WHERE id_user=?";
    var table = ["tabel_penerimaan_biodata_kejuaraan",id_user];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send(error);
        } else {
           res.status(200).json(rows);     
        }
    });
}
exports.getAllBiodata = async function(res){

    var query = "SELECT * FROM ??";
    var table = ["tabel_penerimaan_biodata_kejuaraan"];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send(error);
        } else {
           res.status(200).json(rows);     
        }
    });
}