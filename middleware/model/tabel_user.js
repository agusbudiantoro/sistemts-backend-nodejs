const connection = require('../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../res');
const jwt = require('jsonwebtoken');
const config = require('../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

exports.getDataUser = async function (res,post){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_user", "email", post.email];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                reject(error);
            } else {
                resolve(rows);
            }
        });
    })
};

exports.getAllDataUser = async function (res){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ??"
        var table = ["tabel_user"];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).json({message:error});
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.updateUser = async function(res,data){
    var query = "UPDATE tabel_user SET nama_user=?, no_hp=?, tempat_tgl_lahir=?, alamat=?, jenis_kelamin=?, agama=?, tinggi_badan=?,fakultas_jurusan=?, nim=?, nama_ayah=?, nama_ibu=?, berat_badan=? WHERE id_user=?";
    var table = [data.nama_user,data.no_hp,data.tempat_tgl_lahir,data.alamat,data.jenis_kelamin,data.agama, data.tinggi_badan,data.fakultas_jurusan,data.nim, data.nama_ayah,data.nama_ibu,data.berat_badan, data.id_user];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal update tabel user",value:error })
        } else {
            res.status(200).send("berhasil simpan data");
        }
    });
}

exports.verifUser = async function(res,id_user){
    var query = "UPDATE tabel_user SET status=? WHERE id_user=?";
    var table = [1, id_user];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send({ auth: false, message: "gagal update tabel user",value:error })
        } else {
            res.status(200).send("berhasil verif user");
        }
    });
}

exports.getDataUserById = async function (res,id){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_user", "id_user", id];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(404).json(errors);
            } else {
                res.status(200).json(rows);
            }
        });
    })
};

exports.insertDataUser = async function(res,post){
    return new Promise(function(resolve,reject){
        var query = "INSERT INTO ?? SET ?";
        var table = ["tabel_user"];
        query = mysql.format(query, table);
        connection.query(query, post, function (error, rows) {
            if (error) {
                res.status(404).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}