const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.editJadwal = async function(res,data){

    var query = "UPDATE ?? SET waktu=?, tempat=? WHERE id_jadwal=?";
    var table = ["tabel_jadwal",data.waktu,data.tempat,data.id_jadwal];
    query = mysql.format(query, table);
    connection.query(query, data, function (error, rows) {
        if (error) {
            res.status(404).send(error);
        } else {
           res.status(200).json({value:"berhasil edit data"});     
        }
    });
}