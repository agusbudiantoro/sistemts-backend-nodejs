const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getJadwal = async function(res){

    var query = "SELECT * FROM ??";
    var table = ["tabel_jadwal"];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send(error);
        } else {
           res.status(200).json(rows);     
        }
    });
}