const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.deleteJadwal = async function(res,id_jadwal){

    var query = "DELETE FROM ?? WHERE id_jadwal=?";
    var table = ["tabel_jadwal", id_jadwal];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send(error);
        } else {
           res.status(200).json(rows);     
        }
    });
}