const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.tambahAgenda = async function(res,data){

    var query = "INSERT INTO ?? SET ?";
    var table = ["tabel_agenda"];
    query = mysql.format(query, table);
    connection.query(query, data, function (error, rows) {
        if (error) {
            res.status(404).send(error);
        } else {
           res.status(200).json({value:"berhasil menambahkan data"});     
        }
    });
}