const pathHapus = './upload/foto/';
const fs = require('fs')
const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.hapusAgenda = function (res,id_agenda) {
    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["tabel_agenda", "id_agenda", id_agenda];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).json({pesan:"gagal", error:error});
        } else {
            if (rows.length == 1) {
                const path_file1=rows[0].foto_kegiatan;
                const path_file2= rows[0].foto_pengurus;
                var query1 = "DELETE FROM ?? WHERE ??=?";
                var table1 = ["tabel_agenda", "id_agenda", id_agenda];

                query1 = mysql.format(query1, table1);
                connection.query(query1, function (error, rows) {
                    if (error) {
                        res.status(501).json({pesan:"gagal", error:error});
                    } else {
                        try {
                            fs.unlinkSync(pathHapus+path_file1);
                            fs.unlinkSync(pathHapus+path_file2);
                            //file removed
                          } catch(err) {
                            console.error(err)
                          }
                          res.status(200).json({pesan:"berhasil menghapus data"});
                    }
                })
            }
        }
    });
}