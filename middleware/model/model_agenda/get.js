const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.getAgenda = async function(res){

    var query = "SELECT * FROM ??";
    var table = ["tabel_agenda"];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send(error);
        } else {
           res.status(200).json(rows);     
        }
    });
}

exports.getCountAgendaByIdBidang = async function(id,res){
return new Promise(function(resolve,reject){
    var query = "SELECT * FROM ?? WHERE bidang=?";
    var table = ["tabel_agenda",id];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).send(error);
        } else {
           resolve(rows);  
        }
    });
}); 
}

