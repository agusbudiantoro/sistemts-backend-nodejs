const connection = require('../../koneksi');
const mysql = require('mysql');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const response = require('../../res');
const jwt = require('jsonwebtoken');
const config = require('../../config/secret');
const ip = require('ip');
const multer = require("multer");
const path = require("path");
const { read } = require('fs');

exports.registerAkun = async function(res,post,statusPengguna,pass, idPengguna){
        bcrypt.hash(pass, saltRounds,async function (err, hash) {
            let data = {
                "id_pengguna":idPengguna,
                "email":post.email,
                "password":hash,
                "role":statusPengguna,
                "status":0,
            }
            var query = "INSERT INTO ?? SET ?";
            var table = ["tabel_akun"];
            query = mysql.format(query, table);
            connection.query(query, data, function (error, rows) {
                if (error) {
                    res.status(403).send(error);
                } else {
                    res.status(200).send("berhasil menambahkan data pengguna dan akun");
                }
            });
        });
}

exports.updateIdAkun = async function(res,post,statusPengguna,pass,idPengguna){
    bcrypt.hash(pass, saltRounds,async function (err, hash) {
            var query = "UPDATE tabel_akun SET id_pengguna=?, password=? WHERE id_pengguna=?";
            var table = [idPengguna, hash,idPengguna];
            query = mysql.format(query, table);
            connection.query(query, function (error, rows) {
                if (error) {
                    res.status(404).send({ auth: false, message: "gagal update tabel akun",value:error })
                } else {
                    res.status(200).send("menambahkan data guru berhasil tetapi akun sudah ada");
                }
            });
    });
}

exports.getDataAkun = async function(res,data){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_akun", "email",data.email];

        query = mysql.format(query, table);
        console.log(query);
        connection.query(query, function (error, rows) {
            if(error){
                res.status(403).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}

exports.getDataAkunByIdAkun = async function(id){
    return new Promise(function(resolve,reject){
        var query = "SELECT * FROM ?? WHERE ??=?"
        var table = ["tabel_akun", "id_akun",id];

        query = mysql.format(query, table);

        connection.query(query, function (error, rows) {
            if(error){
                res.status(403).send(error);
            } else {
                resolve(rows);
            }
        });
    });
}