const connection = require('../../../koneksi');
const mysql = require('mysql');
const hapusFile = require('../../controller/hapusFile/main');

exports.editkader = function (res,data) {
    console.log(data);
    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["tabel_penerimaan_kader", "id_kader", data.id_kader];
    query = mysql.format(query, table);

    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).json({pesan:"gagal", error:error});
        } else {
            if (rows.length == 1) {
                console.log(rows);
                const path_file=rows[0].file;
                if(data.file != undefined){
                    var query1 = "UPDATE tabel_penerimaan_kader SET file=? WHERE id_kader=?";
                    var table1 = [data.file, data.id_kader];
                } else {
                    res.status(402).send("form file kosong");
                }
                query1 = mysql.format(query1, table1);
                connection.query(query1, async function (error, rows) {
                    if (error) {
                        res.status(404).json({pesan:"gagal", error:error});
                    } else {
                        try {
                            if(data.file != undefined){
                                let resFile = await hapusFile.hapusFile(path_file);
                            } else{
                                console.log("tidak ubah gambar");
                            }
                            //file removed
                          } catch(err) {
                            console.error(err)
                          }
                          res.status(200).json(rows);
                    }
                })
            }
        }
    });
}


exports.editStatuskader = function (res,data) {
            var query1 = "UPDATE tabel_penerimaan_kader SET status=? WHERE id_kader=?";
            var table1 = [1, data.id_kader];
            
        query1 = mysql.format(query1, table1);
        connection.query(query1, async function (error, rows) {
            if (error) {
                res.status(404).json({pesan:"gagal", error:error});
            } else {
                res.status(200).json(rows);
            }
        })
}