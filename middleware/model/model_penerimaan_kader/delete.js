const pathHapus = './upload/file/';
const fs = require('fs')
const connection = require('../../../koneksi');
const mysql = require('mysql');

exports.hapuskader = function (res,id_kader) {
    var query = "SELECT * FROM ?? WHERE ??=?";
    var table = ["tabel_penerimaan_kader", "id_kader", id_kader];
    query = mysql.format(query, table);
    connection.query(query, function (error, rows) {
        if (error) {
            res.status(404).json({pesan:"gagal", error:error});
        } else {
            if (rows.length == 1) {
                const path_file=rows[0].file;
                var query1 = "DELETE FROM ?? WHERE ??=?";
                var table1 = ["tabel_penerimaan_kader", "id_kader", id_kader];

                query1 = mysql.format(query1, table1);
                connection.query(query1, function (error, rows) {
                    if (error) {
                        res.status(501).json({pesan:"gagal", error:error});
                    } else {
                        try {
                            fs.unlinkSync(pathHapus+path_file);
                            //file removed
                          } catch(err) {
                            console.error(err)
                          }
                          res.status(200).json({pesan:"berhasil menghapus data"});
                    }
                })
            }
        }
    });
}